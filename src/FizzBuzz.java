
public class FizzBuzz {

    public static void fizzBuzz(String value){

    	if(isInt(value)) {

            String out = "";
            int i = Integer.parseInt(value);

            if(i % 3 ==  0){
                out = "Fizz";
            }
            if(i % 5 == 0){
                out += "Buzz";
            }
            else if(out.length() == 0){
                out += i;
            }

            System.out.println(out);
    	}
    }

    static public boolean isBlank(String value) {
        if (value == null || value.length() == 0) {
            return true;
        }
        return false;
    }

    static public boolean isInt(String value) {
        if (isBlank(value)) {
            return false;
        }
        try {
            Integer.parseInt(value);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

}
